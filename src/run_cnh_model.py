# External Modules
import cv2 as cv
import pytesseract as ocr
import time
import redis
import json
import time
from imutils.object_detection import non_max_suppression
# Internal Modules
from east_model import TextDetection
import utils as util
import settings



#----------------REGIONS----------------#
fields = {}
fields["cpf"] = [0.486656 , 0.208316,  0.752112, 0.252163]
fields["filiacao"] = [ 0.486231, 0.260653 ,  0.949810, 0.377333]
fields["nome"] = [0.152376 , 0.114944 , 0.945088 , 0.161999]
fields["validade"] = [0.474110 , 0.431085 , 0.709490 , 0.476382]
#----------------------------------------#

def load_east_model():
    model = TextDetection("model/frozen_east_text_detection.pb")
    return model

def read_cnh(img, model):
    """Read fields of cnh images
        input:
            img: ndarray
        return: 
            dict
    """
    #wraped = crop_cnh(img)
    crop = img
    
    # Get upper page
    doc = crop[:int(crop.shape[0]/2*1.05),:,]
    # Remove blur
    sharpen = util.unsharp_mask(doc, amount = 1.7)
    img = util.unsharp_mask(img, amount = 1.7)
    # Remove left bart and upper bar
    sharpen, thresh_y = util.remove_upper_bar(sharpen)
    
    # Run text region detection
    boxes = model.findTextRegions(sharpen, 640,640)
    result_sharp = sharpen.copy()
    
    for box in boxes:
        box[1] += thresh_y
        box[-1] += thresh_y
    
    result = {}
    #Nome
    nome = util.crop_roi(crop, fields["nome"], boxes)
    if  len(nome) > 0:
        nome = util.unsharp_mask(nome, amount = 1.2)
        custom_config = r'-c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyz --psm 6'
        result["nome"] = filter_nome(ocr.image_to_string(nome, config = custom_config))
    else:
        result["nome"] = ""
    
    #CPF
    cpf = util.crop_roi(crop, fields["cpf"], boxes)
    if  len(cpf) > 0:
        cpf = util.unsharp_mask(cpf, amount = 1.2)
        custom_config = r'--oem 3 --psm 6 outputbase digits'
        result["cpf"] = filter_cpf(ocr.image_to_string(cpf, config = custom_config))
    else:
        result["cpf"] = ""
    
    #Filiacao
    filiacao = util.crop_roi(crop, fields["filiacao"], boxes)
    if  len(filiacao) > 0:
        filiacao = util.unsharp_mask(filiacao, amount = 1.2)
        custom_config = r'-c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyz --psm 6'
        result["filiacao"] = filter_filiacao(ocr.image_to_string(filiacao, config = custom_config))
    else:
        result["filiacao"] = ""
         
    #Validade    
    validade = util.crop_roi(crop, fields["validade"], boxes)
    if  len(validade) > 0:
        validade = util.unsharp_mask(validade, amount = 1.2)
        custom_config = r'--oem 3 --psm 6 outputbase digits'
        result["validade"] = filter_validade(ocr.image_to_string(validade, config = custom_config))
    else:
        result["validade"] = ""
    return result

def crop_cnh(img):
    """Crop CNH from image
        input:
            img: ndarray
        return: 
            img: ndarray
    """
    rat_h = img.shape[0] / 550.0
    rat_w = img.shape[1] / 550.0
    resized = cv.resize(img, (550,550), interpolation = cv.INTER_AREA) 
    edged = util.filter_doc(resized)
    dots,box,cnt = util.find_corners_dots(edged, debug = True)
    for x in range(0, len(dots)):
        dots[x] = (int(dots[x][0] * rat_w) , int(dots[x][1] * rat_h))
    wraped = util.four_point_transform(img, dots)
    
    # Turm image 90 degree if necessary
    if util.is_horizontal(wraped):
        wraped = util.rotate_bound(wraped, 90)
    # Resize image
    crop = cv.resize(wraped, (550,800), cv.INTER_AREA)
    # Check if the img is inverse 
    if util.is_inverse(crop):
        crop = util.rotate_bound(crop, -180)
    
    return crop

def letters(string):
    for c in string:
        if not str.isalpha(c):
            string = string.replace(c, " ")
    return string

def numbers(string):
    valid = ["0","1","2","3","4","5","6","7","8","9","/",",",".","-","~"]
    for c in string:
        if c not in valid:
            string = string.replace(c," ")
    return string

def filter_nome(string):
    temp = letters(string).lower()
    final = ""
    for block in temp.split(" "):
        if len(block) > 3:
            final += block
        elif block in ["da","de","do" ,"dos", "e"]:
            final += block
        final += " "
    return final

def filter_cpf(string):
    temp = numbers(string)
    temp = temp.replace(",", ".")
    temp = temp.replace("~", "-")
    if len(temp.split(".")[0]) > 3:
        temp = temp[len(temp.split(".")[0]) - 3:]
    if len(temp.split("-")[-1]) > 2:
        temp = temp[:len(temp.split("-")[0]) + 3] 
    return temp.replace(" ", "")

def filter_filiacao(string):
    temp = letters(string).lower()
    final = ""
    for block in temp.split(" "):
        if len(block) > 3:
            final += block
        elif block in ["da","de","do" ,"dos", "e"]:
            final += block
        final += " "
    return final

def filter_validade(string):
    temp = numbers(string)
    temp = temp.replace(" ", "")
    if len(temp.split("/")) < 3:
        temp = temp.replace(",","/")
    if len(temp.split("/")[0]) > 2:
        temp = temp[len(temp.split("/")[0]) - 2:]
    if len(temp.split("/")[-1]) > 4:
        temp = temp[:10]
    return temp

# connect to Redis server
db = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

def cnh_process():
    print("* Loading model")
    model = load_east_model()
    print(" model loaded")
    while True:
        queue = db.lrange(settings.CNH_IMAGE_QUEUE, 0, settings.BATCH_SIZE - 1)
        imageIDs = []
        batch = None
        
        for q in queue:
            q = json.loads(q.decode("utf-8"))
            image = util.base64_decode_image(q["image"],
                                             (settings.CNH_IMAGE_HEIGHT, 
                                             settings.CNH_IMAGE_WIDTH,settings.CNH_IMAGE_CHANS))
            if batch is None:
                batch = [image]
            else:
                batch.append(image)
            imageIDs.append(q["id"])
        if len(imageIDs) > 0:
            print("* Batch size: " + str(len(batch)))
            for (imageID, image) in zip(imageIDs, batch):
                result = read_cnh(image, model)
                db.set(imageID, json.dumps(result))
            db.ltrim(settings.CNH_IMAGE_QUEUE, len(imageIDs), -1)
        time.sleep(settings.SERVER_SLEEP)

# process
if __name__ == "__main__":
	cnh_process()
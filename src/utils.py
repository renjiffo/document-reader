import cv2 as cv
import numpy as np
import base64
import sys
from scipy.signal import find_peaks,peak_widths

def is_horizontal(img): 
    """Check if doc are in horizontal position
    input:
        img: ndarray
    return: 
        bool
    """
    if img.shape[0] < img.shape[1]:
        return True
    return False

def unsharp_mask(image, kernel_size=(5, 5), sigma=1.0, amount=1.0, threshold=0):
    """unblur img by enhancing the edge contrast
        input:
            image: ndarray
            kernel_size: (heigth, width)
            sigma: float
            amount: float
            threshold: int
        return:
            sharpened: ndarray
    """
    blurred = cv.GaussianBlur(image, kernel_size, sigma)
    sharpened = float(amount + 1) * image - float(amount) * blurred
    sharpened = np.maximum(sharpened, np.zeros(sharpened.shape))
    sharpened = np.minimum(sharpened, 255 * np.ones(sharpened.shape))
    sharpened = sharpened.round().astype(np.uint8)
    if threshold > 0:
        low_contrast_mask = np.absolute(image - blurred) < threshold
        np.copyto(sharpened, image, where=low_contrast_mask)
    return sharpened

def remove_upper_bar(img):
    """crop the upper bar of CNH doc
        input:
            img: ndarray
        return:
            img: ndarray
    """
    imgray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ret, otsu = cv.threshold(imgray,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    otsu = cv.bitwise_not(otsu)
    dilatation_dst = cv.dilate(otsu, kernel = np.array([1,1,1,1]))
    dilatation_dst = cv.dilate(dilatation_dst, kernel = np.array([1,1,1,1]))
    img_row_sum = np.sum(dilatation_dst,axis=1).tolist()
            
    thes = np.quantile(img_row_sum, 0.7)
    thes_min = np.quantile(img_row_sum, 0.69)
    x = int(len(img_row_sum)*0.10)
    # jump background
    while img_row_sum[x] < thes_min and x < len(img_row_sum):
        x += 1
    # Get the end of the upper bar    
    while img_row_sum[x] >= thes and x < len(img_row_sum) - 1:
        x += 1
    return img[x:,:,:],x

def rotate_bound(image, angle):
    """rotate image and reshape img if necesary
        input:
            image: ndarray
            angle: float
        return:
            image: ndarray
    """
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    # perform the actual rotation and return the image
    return cv.warpAffine(image, M, (nW, nH))


def remove_vertical_bars(img):
    """remove vertical columns of image
        input:
            img: ndarray
        return:
            img: ndarray
    """
    img_col_sum = np.sum(img, axis=0).tolist()
    temp = max(img_col_sum)
    zeros = np.zeros(img.shape[0])
    for x in range(0,len(img_col_sum)):
        if img_col_sum[x] >= int(0.95 * temp):
            img[:,x] = zeros
    return img

def clean_horizontal_noisy(img):
    """remove noisy pixel blocks upper and lower of text
        input:
            img: ndarray
        return:
            img: ndarray
    """
    img_row_sum = np.sum(img, axis=1).tolist()
    zeros = np.zeros(img.shape[1])
    peaks, _ = find_peaks(img_row_sum)
    result = peak_widths(img_row_sum, peaks, rel_height = 1)
    c = result[0].tolist().index(max(result[0]))
    
    for x in range(0, int(result[2][c]) - 1):
        img[x,:] = zeros
    for x in range(int(result[3][c]) + 1, len(img_row_sum)) :
        img[x,:] = zeros
            
    return img



def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
	# return the ordered coordinates
	return rect

def four_point_transform(image, ptss):
    pts = np.array(ptss, dtype = "float32")
    # obtain a consistent order of the points and unpack them
	# individually
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")
    # compute the perspective transform matrix and then apply it
    M = cv.getPerspectiveTransform(rect, dst)
    warped = cv.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped


def filter_doc(img, k = 5, minCanny = 85, maxCanny = 200):
    """filter the contour of a document
        input:
            img: ndarray
            k: uint
            minCanny: uint
            maxanny: uint
        return:
            edged: ndarray
    """
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray = cv.GaussianBlur(gray, (5, 5), 0)
    edged = cv.Canny(gray, minCanny, maxCanny)

    kernel = np.ones((1,k),np.uint8)
    edged = cv.dilate(edged, kernel, iterations = 1)
    kernel = np.ones((k,1),np.uint8)
    edged = cv.dilate(edged, kernel, iterations = 1)
    
    return edged

def taxi_dist(a,b):
    """ Taxi distance between two points
        input:
            a: (int,int)
            b: (int,int)
        return:
            dist: int
    """
    return  np.abs(a[0] - b[0]) + np.abs(a[1] - b[1])

def find_corners_dots(img, debug = False):
    """ Find the the closest dots of each dot of the bounding box
        input:
            img: ndarray
        return:
            dots: [(int,int),(int,int),(int,int),(int,int)]
    """
    # find contour points
    contours, hierarchy = cv.findContours(img, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    cntsSorted = sorted(contours, key=lambda x: cv.contourArea(x))
    
    # find corners of contour
    epsilon = 0.5*cv.arcLength(cntsSorted[-1],True)
    approx = cv.approxPolyDP(cntsSorted[-1],epsilon,True)
    rect = cv.minAreaRect(cntsSorted[-1])
    box = cv.boxPoints(rect)
    box = np.int0(box)
    
    dots = []
    cnt = cntsSorted[-1]
    for dot in box:
        min_dist = img.shape[0] + img.shape[1]
        x = 0
        y = 0 
        for point in cnt:
            if taxi_dist(point[0], dot) < min_dist:
                x = point[0][0]
                y = point[0][1]
                min_dist = taxi_dist(point[0], dot)
        dots.append(( x, y))
    if debug:
        return dots,box,cnt
    return dots

def is_inverse(img):
    """Check if CNH image is upside down
        input:
            img: ndarray
        return: bool
    """
    A = img[:int(img.shape[0]/2*1.05),:,]
    imgray = cv.cvtColor(A, cv.COLOR_RGB2GRAY)
    ret, otsu = cv.threshold(imgray,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    otsu = cv.bitwise_not(otsu)
    hist_white_a = cv.calcHist([otsu],[0],None,[256],[0,256])[-1]
    
    B = img[int(img.shape[0]/2*1.05)+ 1:,:,]
    imgray = cv.cvtColor(B, cv.COLOR_RGB2GRAY)
    ret, otsu = cv.threshold(imgray,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    otsu = cv.bitwise_not(otsu)
    hist_white_b = cv.calcHist([otsu],[0],None,[256],[0,256])[-1]
    
    if hist_white_a > hist_white_b:
        return False
    return True


def isInTheBox(roi, box):
    """Check if roi is in box
        input:
            roi: [int, int, int, int]
            box: [int, int, int, int]
        return:
            bool
    """
    for x in range(0,4,2):
        if box[0 + x] > roi[0] and box[0 + x] < roi[2]:
            if box[1 + x] > roi[1] and box[1 + x] < roi[-1]:
                return True
    return False



def crop_roi(img, field, boxes):
    """Crop ROI from img
        input:
            img: ndarray
            name: string
            boxes: list of [int,int,int,int]
        return:
            img: ndarray
    """

    H = img.shape[0]
    W = img.shape[1]
    
    
    x1 = int(W * field[0])
    x2 = int(W * field[2])
    y1 = int(H * field[1])
    y2 = int(H * field[-1])
    roi = [x1,y1,x2,y2]

    
    selected_boxes = []
    H = 0
    heights = []
    for box in boxes:
        if isInTheBox(roi,box):
            if box[2] > roi[2] and np.abs(roi[2] - box[2])/np.abs(box[0] - box[2]) > 0.5:
                continue
            else:
                selected_boxes.append(box)
                heights.append(np.abs(box[1] - box[-1]))
    
    y_min = []
    y_max = []
    x_min = []
    x_max = []

    
    if len(heights) == 0:
        return np.zeros(0)
    
    # Filter box by median height
    H = np.median(heights)
    for box in selected_boxes:
        if np.abs(box[1] - box[-1]) >= H and np.abs(int((box[1] + box[-1])/2) - int((y1 + y2)/2)) < 0.48*(np.abs(y1 - y2)):
            y_min.append(min(box[1] , box[-1]))
            y_max.append(max(box[1] , box[-1]))
            x_min.append(min(box[0] , box[2]))
            x_max.append(max(box[0] , box[2]))
    
    if len(y_min) == 0:
        return np.zeros(0)
    alpha = int(0.25 * H)
    return img[min(y_min) - alpha:max(y_max) + alpha,min(x1, min(x_min)) - alpha:max(x2, max(x_max)) + alpha,]


def base64_encode_image(a):
	# base64 encode the input NumPy array
	return base64.b64encode(a).decode("utf-8")

def base64_decode_image(a, shape):
	# if this is Python 3, we need the extra step of encoding the
	# serialized NumPy string as a byte object
	if sys.version_info.major == 3:
		a = bytes(a, encoding="utf-8")
	# convert the string to a NumPy array using the supplied data
	# type and target shape
	a = np.frombuffer(base64.decodestring(a), dtype = np.uint8)
	a = a.reshape(shape)
	# return the decoded image
	return a
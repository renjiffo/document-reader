# initialize Redis connection settings
REDIS_HOST = "localhost"
REDIS_PORT = 6379
REDIS_DB = 0

# ---------------CNH MODEL SETUP---------------#
# data type
CNH_IMAGE_WIDTH = 550
CNH_IMAGE_HEIGHT = 800
CNH_IMAGE_CHANS = 3

# initialize constants used for server queuing
CNH_IMAGE_QUEUE = "cnh_images_queue"
BATCH_SIZE = 25
SERVER_SLEEP = 0.25
CLIENT_SLEEP = 0.25
#----------------------------------------------#

# ---------------REN MODEL SETUP---------------#
#----------------------------------------------#
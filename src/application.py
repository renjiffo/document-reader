# External Modules
import numpy as np
import cv2 as cv
from PIL import Image
import flask
import redis
import uuid
import io
import json
import time
from threading import Thread
# Internal Moules
import utils as util 
from run_cnh_model import crop_cnh
from run_cnh_model import cnh_process
import settings

# initialize our Flask application and Redis server
application = app = flask.Flask(__name__)
db = redis.StrictRedis(host = settings.REDIS_HOST, port = settings.REDIS_PORT, db = settings.REDIS_DB)


@app.route("/cnhRead", methods = ["POST"])
def predict():
    data = {"success": False}

    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            image = flask.request.files["image"].read()
            image = Image.open(io.BytesIO(image)).convert('RGB')
            open_cv_image = np.array(image) 
            # Convert RGB to BGR 
            open_cv_image = open_cv_image[:, :, ::-1].copy()    
            crop_img = crop_cnh(open_cv_image)
            
            # ensure our NumPy array is C-contiguous as well,
			# otherwise we won't be able to serialize it
            image = crop_img.copy(order = "C")
			
            # generate an ID for the classification then add the
			# classification ID + image to the queue
            k = str(uuid.uuid4())
            image = util.base64_encode_image(image)
            d = {"id": k, "image": image}
            db.rpush(settings.CNH_IMAGE_QUEUE, json.dumps(d))
            
			# keep looping until our model server returns the output
			# predictions
            while True:
				# attempt to grab the output predictions
                output = db.get(k)
				# check to see if our model has classified the input
				# image
                if output is not None:
					# add the output predictions to our data
					# dictionary so we can return it to the client
                    output = output.decode("utf-8")
                    data["result"] = json.loads(output)
					# delete the result from the database and break
					# from the polling loop
                    db.delete(k)
                    break
				# sleep for a small amount to give the model a chance
				# to classify the input image
                time.sleep(settings.CLIENT_SLEEP)
			# indicate that the request was a success
            data["success"] = True
	
    # return the data dictionary as a JSON response
    return flask.jsonify(data)

# for debugging purposes, it's helpful to start the Flask testing
# server (don't use this for production
if __name__ == "__main__":
    print("* Starting cnh reader service...")
    t = Thread(target=cnh_process, args=())
    t.daemon = True
    t.start()
    # start the web server
    print("* Starting web service...")
    app.run()
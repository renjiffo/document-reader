from imutils.object_detection import non_max_suppression
import cv2 as cv
from PIL import Image
import numpy as np
import sys

class TextDetection:
    def __init__(self, path):
        try:
            self.net = cv.dnn.readNet(path)
        except:
            print("failed to load east_model .pb file")
            sys.exit()
        
    def findTextRegions(self, img, w, h):
        
        # Save img original size and aspect ratio
        (H, W) = img.shape[:2]

        # Get new params
        (newW, newH) = (w, h)
        rW = W / float(newW)
        rH = H / float(newH)
        
        # resize the image and grab the new image dimensions
        img = cv.resize(img, (newW, newH))
        (H, W) = img.shape[:2]
  
        layerNames = ["feature_fusion/Conv_7/Sigmoid",
                      "feature_fusion/concat_3"]
        
        # Convert img to the format of CNN
        blob = cv.dnn.blobFromImage(img, 1.0, (W, H),(123.68, 116.78, 103.94), swapRB = True, crop = False)
        self.net.setInput(blob)
        
        # Run model of input
        (scores, geometry) = self.net.forward(layerNames)

        (numRows, numCols) = scores.shape[2:4]
        
        # Save bounding boxes
        rects = []
        confidences = []
        for y in range(0, numRows):
            scoresData = scores[0, 0, y]
            xData0 = geometry[0, 0, y]
            xData1 = geometry[0, 1, y]
            xData2 = geometry[0, 2, y]
            xData3 = geometry[0, 3, y]
            anglesData = geometry[0, 4, y]

            # loop over the number of columns
            for x in range(0, numCols):
                # if our score does not have sufficient probability, ignore it
                if scoresData[x] < 0.95:
                    continue
                # compute the offset factor as our resulting feature maps will
                # be 4x smaller than the input image
                (offsetX, offsetY) = (x * 4.0, y * 4.0)
                # extract the rotation angle for the prediction and then
                # compute the sin and cosine
                angle = anglesData[x]
                cos = np.cos(angle)
                sin = np.sin(angle)
                # use the geometry volume to derive the width and height of
                # the bounding box
                h = xData0[x] + xData2[x]
                w = xData1[x] + xData3[x]
                # compute both the starting and ending (x, y)-coordinates for
                # the text prediction bounding box
                endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
                endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
                startX = int(endX - w)
                startY = int(endY - h)
                # add the bounding box coordinates and probability score to
                # our respective lists
                rects.append((startX, startY, endX, endY))
                confidences.append(scoresData[x])
        
            # apply non-maxima suppression to suppress weak, overlapping bounding
            # boxes
            boxes = non_max_suppression(np.array(rects), probs=confidences)
           
        # loop over the bounding boxes
        roi_boxes = []
        for (startX, startY, endX, endY) in boxes:
            # scale the bounding box coordinates based on the respective
            # ratios
            startX = int(startX * rW)
            startY = int(startY * rH)
            endX = int(endX * rW)
            endY = int(endY * rH)
            roi_boxes.append([startX, startY, endX, endY])
        
        return roi_boxes